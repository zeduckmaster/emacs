;;; -*- no-byte-compile: t -*-
(define-package "parent-mode" "2.3" "get major mode's parent modes" 'nil :commit "db692cf08deff2f0e973e6e86e26662b44813d1b" :url "https://github.com/Fanael/parent-mode")
