;;; -*- no-byte-compile: t -*-
(define-package "highlight-numbers" "0.2.3" "Highlight numbers in source code" '((emacs "24") (parent-mode "2.0")) :commit "b7adef0286aaa5bca8e98a12d0ffed3a880e25aa" :authors '(("Fanael Linithien" . "fanael4@gmail.com")) :maintainer '("Fanael Linithien" . "fanael4@gmail.com") :url "https://github.com/Fanael/highlight-numbers")
