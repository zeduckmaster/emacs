(define-package "ox-hugo" "0.12.2" "Hugo Markdown Back-End for Org Export Engine"
  '((emacs "26.3")
    (tomelr "0.4.3"))
  :commit "af3d0cdf8c672be498d54bc9efdd0e40b2528602" :authors
  '(("Kaushal Modi" . "kaushal.modi@gmail.com")
    ("Matt Price" . "moptop99@gmail.com"))
  :maintainer
  '("Kaushal Modi" . "kaushal.modi@gmail.com")
  :keywords
  '("org" "markdown" "docs")
  :url "https://ox-hugo.scripter.co")
;; Local Variables:
;; no-byte-compile: t
;; End:
