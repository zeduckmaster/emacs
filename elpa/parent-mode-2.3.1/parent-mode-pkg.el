(define-package "parent-mode" "2.3.1" "get major mode's parent modes" 'nil :commit "9fe5363b2a190619641c79b3a40d874d8c8f9f40" :authors
  '(("Fanael Linithien" . "fanael4@gmail.com"))
  :maintainers
  '(("Fanael Linithien" . "fanael4@gmail.com"))
  :maintainer
  '("Fanael Linithien" . "fanael4@gmail.com")
  :url "https://github.com/Fanael/parent-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
