(define-package "doom-modeline" "3.3.1" "A minimal and modern mode-line"
  '((emacs "25.1")
    (compat "28.1.1.1")
    (shrink-path "0.2.0"))
  :commit "156b02445c3360added80009ab3c1a33dd88c5d9" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:
