(deftheme visual
  "Face colors using a vs style.")

(let ((class '((class color) (min-colors 89)))
      (c_bg       "#1c1c1c") ;;gray11
      (c_fg       "#dcdcdc") ;;gainsboro
      (c_selec    "#103050") ;;
      (c_head1    "#87cefa") ;;light sky blue
      (c_head2    "#98f5ff") ;;CadetBlue1
      (c_head3    "#cae1ff") ;;LightSteelBlue1
      (c_head4    "#00e5ee") ;;turquoise2
      (c_head5    "#7fffd4") ;;aquamarine1
      (c_err      "#ff0000") ;;red
      (c_warn     "#ffff00") ;;yellow
      (c_succ     "#4eee94") ;;SeaGreen2
      (c_comment  "#7f7f7f") ;;gray50
      (c_constant "#B8D7A3") ;;pale green?
      (c_preproc  "#9c9c9c") ;;gray61
      (c_str      "#d69d85") ;;light salmon?
      (c_keyword  "#569cd6") ;;
      (c_delim    "#00ffff") ;;cyan
      (c_type     "#4ec9b0") ;;
      (c_num      "#f08080") ;;light coral
      (c_prompt   "#98f5ff") ;;CadetBlue1
      (c_snipp    "#afeeee") ;;pale turquoise
      
	  (bg2 "black")
	  (bg3 "#2D2D30")
	  (bg4 "#3F3F46")
	  (bgselec "#103050")
	  (fg1 "#DCDCDC")
	  (fg2 "white")
	  (prompt "CadetBlue1")
	  (num "LightCoral")
      )
  
  (custom-theme-set-faces
   'visual

   ;; base ===========================================================
   `(cursor ((,class (:background "white"))))
   `(default ((,class (:background ,c_bg :foreground ,c_fg))))
   `(error ((,class (:foreground ,c_err))))
   `(font-lock-builtin-face ((,class (:foreground ,c_fg))))
   `(font-lock-comment-delimiter-face ((,class (:foreground ,c_comment))))
   `(font-lock-comment-face ((,class (:foreground ,c_comment))))
   `(font-lock-constant-face ((,class (:foreground ,c_constant))))
   ;;`(font-lock-doc-face ((,class (:foreground "moccasin"))))
   ;;`(font-lock-doc-string-face ((,class (:foreground "moccasin"))))
   `(font-lock-function-name-face ((,class (:foreground ,c_fg))))
   `(font-lock-keyword-face ((,class (:foreground ,c_keyword))))
   `(font-lock-preprocessor-face ((,class (:foreground ,c_preproc))))
   `(font-lock-reference-face ((,class (:foreground ,c_num))))
   `(font-lock-regexp-grouping-backslash ((,class (:weight bold))))
   `(font-lock-regexp-grouping-construct ((,class (:weight bold))))
   `(font-lock-string-face ((,class (:foreground ,c_str))))
   `(font-lock-type-face ((,class (:foreground ,c_type))))
   `(font-lock-variable-name-face ((,class (:foreground ,c_fg))))
   `(highlight-numbers-number ((,class (:foreground ,c_num))))
   ;;`(highlight-operators-face ((, class (:foreground "cyan"))))
   `(hl-line ((,class (:background "black"))))
   `(ido-first-match ((,class (:weight normal :foreground "orange"))))
   `(ido-only-match ((,class (:foreground "green"))))
   `(ido-subdir ((,class (:foreground nil :inherit font-lock-keyword-face))))
   `(line-number ((,class (:foreground ,c_comment :height 0.75))))
   `(line-number-current-line ((,class (:foreground ,c_comment :background ,bg2 :height 0.75))))
   ;;`(match ((,class (:background "DeepPink4"))))
   `(minibuffer-prompt ((,class (:foreground "CadetBlue1"))))
   `(outline-1 ((,class (:foreground ,c_head1 :weight bold :height 1.5))))
   `(outline-2 ((,class (:foreground ,c_head2 :weight bold :height 1.25))))
   `(outline-3 ((,class (:foreground ,c_head3 :height 1.15))))
   `(outline-4 ((,class (:foreground ,c_head4 :height 1.1))))
   `(outline-5 ((,class (:foreground ,c_head5))))
   ;;`(primary-selection ((,class (:background "blue3"))))
   `(region ((,class (:background ,c_selec))))
   ;;`(show-paren-match-face ((,class (:background "dodgerblue1" :foreground "white"))))
   ;;`(show-paren-mismatch-face ((,class (:background "red1" :foreground "white"))))
   `(success ((,class (:foreground ,c_succ))))
   `(warning ((,class (:foreground ,c_warn))))

   ;; dired mode ====================================================
   `(dired-directory ((,class (:inherit ido-subdir))))
   
   ;; mode line ======================================================
   `(mode-line ((,class (:background "#0d0d0d" :foreground "#bbc2cf"))))
   `(mode-line-inactive ((,class (:background "#242424" :foreground "#5B6268"))))
   ;;`(mode-line-buffer-id ((,class (:weight normal :background nil :foreground "gray11"))))
   `(mode-line-emphasis ((,class (:foreground "#51afef"))))

   ;; doom mode line =================================================
   `(doom-modeline-buffer-file ((,class (:inherit mode-line-buffer-id :weight bold))))
   `(doom-modeline-buffer-path ((,class (:inherit mode-line-emphasis :weight bold))))
   `(doom-modeline-buffer-project-root ((,class (:foreground "green" :weight bold))))
   `(doom-modeline-bar ((,class (:background "#51afef"))))

   ;; org mode =======================================================
   ;;`(org-document-title ((,class (:inherit default :height 2.0))))
   `(org-verbatim ((,class (:foreground ,c_type))))
   `(org-code ((,class (:foreground ,c_type))))

   ;; markdown =======================================================
   `(markdown-header-face ((,class (:foreground ,c_head1))))
   `(markdown-header-delimiter-face ((,class (:foreground ,c_head1))))

   ;; nxml ===========================================================
   `(nxml-element-local-name ((,class (:foreground ,c_keyword))))
   `(nxml-tag-delimiter ((,class (:foreground ,c_keyword))))
   ))

(provide-theme 'visual)

