;;; init-clang-format.el
(require 'clang-format)
(global-set-key [C-M-tab] 'clang-format-region)
(add-hook 'c-mode-common-hook
          (function (lambda()
                      (add-hook 'before-save-hook
                                'clang-format-buffer)
                      )
                    )
          )

(when (equal system-type 'windows-nt)
  (setq clang-format-executable "~/Desktop/tools/LLVM/bin/clang-format.exe")
  )

(provide 'init-clang-format)
;;; init-clang-format.el ends here
