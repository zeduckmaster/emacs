;;; init-locales.el
(setenv "LANG" "en_US.UTF-8")

(setq-default calendar-week-start-day 1)

(prefer-coding-system 'utf-8)
;(set-default-coding-systems 'utf-8)
;(set-terminal-coding-system 'utf-8)

(provide 'init-locales)
;;; init-locales.el ends here
