;;; init-markdown
;;(require 'markdown-mode)
(add-hook 'markdown-mode-hook
          (lambda()
            (setq indent-tabs-mode nil)
            )
          )

(provide 'init-markdown)
;;; init-markdown.el ends here
