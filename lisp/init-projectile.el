;;; init-projectile.el
(require 'projectile)

;; Recommended keymap prefix on Windows/Linux
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(setq
 projectile-auto-discover nil
 projectile-track-known-projects-automatically nil
 projectile-require-project-root 1
 projectile-enable-caching 1
 )

(projectile-mode 1)

(provide 'init-projectile)
;;; init-projectile.el ends here
