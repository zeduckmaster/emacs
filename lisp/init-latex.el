;;; init-latex.el
(when (equal system-type 'windows-nt)
  (add-to-list 'exec-path "~/Desktop/tools/miktex/texmfs/install/miktex/bin/x64")
;;  (setq org-latex-pdf-process
;;        '("lualatex -shell-escape -interaction nonstopmode %f"
;;          "lualatex -shell-escape -interaction nonstopmode %f")
;;        )
  )

(provide 'init-latex)
;;; init-latex.el ends here
