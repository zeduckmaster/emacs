;;; init-dired.el

(put 'dired-find-alternate-file 'disabled nil)

(add-hook 'dired-mode-hook
          (lambda ()
            (dired-hide-details-mode)
            ))

(provide 'init-dired)
;;; init-dired.el ends here
