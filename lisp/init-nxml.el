;;; init-nxml.el
(setq
 nxml-child-indent 4
 nxml-attribute-indent 4)

(provide 'init-nxml)
;;; init-nxml.el ends here
