;;; init-spelling.el
(setenv "DICPATH"
        (concat (getenv "HOME") "/.emacs.d/myspell"))

(setq ispell-dictionary "en_US")

(setq ispell-hunspell-dict-paths-alist
      '(("en_US" "~/.emacs.d/myspell/en_US.aff")
        ("en_GB" "~/.emacs.d/myspell/en_GB.aff")
        ("fr_FR" "~/.emacs.d/myspell/fr_FR.aff")))

(provide 'init-spelling)
;;; init-spelling.el ends here
