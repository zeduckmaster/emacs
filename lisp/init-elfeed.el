;;; init-elfeed.el
(require 'elfeed)

(setq elfeed-feeds
      '(("http://feeds.feedburner.com/GamasutraNews" tech)
        ("https://www.phoronix.com/rss.php" tech)
        ("https://www.nextinpact.com/rss/news.xml" tech)
        ("https://www.qt.io/blog/rss.xml" tech)
        ("https://feeds.feedburner.com/nvidiablog" tech)
        ("https://www.gamedev.net/news/?rss=1" tech)
        ("https://www.gamekult.com/feed.xml" vg)
        ("https://kotaku.com/rss" vg)
        ("https://www.black-book-editions.fr/rss.xml" games)
        ("https://www.rpgnews.com/feed/rss/" games))
      )

(provide 'init-elfeed)
;;; init-elfeed.el ends here
