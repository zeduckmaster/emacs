;;; init-editor.el

;; editor behavior
(setq-default
 tab-width 4
 indent-tabs-mode nil
 c-default-style "bsd"
 c-basic-offset 2
 c-indent-level 2
 mouse-wheel-scroll-amount (quote (1 ((shift) . 1) ((control))))
 )
(delete-selection-mode 1)
(ido-mode 1)
(global-set-key (kbd "C-c SPC") 'mc/edit-lines)

;; move line up and down
(defmacro save-column (&rest body)
  `(let ((column (current-column)))
     (unwind-protect
         (progn ,@body)
       (move-to-column column))))
(put 'save-column 'lisp-indent-function 0)

(defun move-line-up ()
  (interactive)
  (save-column
    (transpose-lines 1)
    (forward-line -2)))

(defun move-line-down ()
  (interactive)
  (save-column
    (forward-line 1)
    (transpose-lines 1)
    (forward-line -1)))
(global-set-key (kbd "<M-up>") 'move-line-up)
(global-set-key (kbd "<M-down>") 'move-line-down)

;; editor colors
(add-to-list 'custom-theme-load-path (expand-file-name "themes" user-emacs-directory))
(load-theme 'visual t)

;; editor fonts
(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-12"))
(when (display-graphic-p)
  (set-frame-font "DejaVu Sans Mono-12" nil t)
  ;;(require 'all-the-icons)
  (setq inhibit-compacting-font-caches t)
  )

;; some cosmetics adjustments
(doom-modeline-mode 1)
(column-number-mode 1)
(add-hook 'prog-mode-hook
          (lambda()
            (highlight-numbers-mode 1)
            ))
(global-hl-line-mode 1)
(global-visual-line-mode 1)

;(defcustom display-line-numbers-exempt-modes '(
;                                               ansi-term-mode
;                                               dired-mode
;                                               doc-view-mode
;                                               eshell-mode
;                                               org-mode
;                                               shell-mode
;                                               term-mode
;                                               treemacs-mode
;                                               vterm-mode
;                                               )
;  "Major modes on which to disable the linum mode, exempts them from global requirement"
;  :group 'display-line-numbers
;  :type 'list
;  :version "green")
;
;(defun display-line-numbers--turn-on ()
;  "turn on line numbers but exempting certain major modes defined in `display-line-numbers-exempt-modes'"
;  (if (and
;       (not (member major-mode display-line-numbers-exempt-modes))
;       (not (minibufferp)))
;      (display-line-numbers-mode)))
;
;(global-display-line-numbers-mode)
(defun display-line-numbers--hook ()
  (display-line-numbers-mode 1)
  )
(add-hook 'prog-mode-hook 'display-line-numbers--hook)
;(add-hook 'text-mode-hook 'display-line-numbers--hook)


(provide 'init-editor)
;;; init-editor.el ends here
