;;; init-org.el
(setq
 org-hide-emphasis-markers t
 org-hide-leading-stars t
 org-src-fontify-natively t
 org-src-tab-acts-natively t
 org-edit-src-content-indentation 0
 org-tags-column -56
 )

;; nicer bullets
;(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; org-mode languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (octave . t)
   (plantuml . t)
   (python . t)
   ))

;; automatic refresh of inline images
(eval-after-load 'org
  (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images))

;; no query for evaluate
(defun my-org-confirm-babel-evaluate (lang body)
  (not (string= lang "python")))
(setq org-confirm-babel-evaluate #'my-org-confirm-babel-evaluate)

;; ox-hugo
(when (display-graphic-p)
  (with-eval-after-load 'ox
    (require 'ox-hugo))
  )

;; ox-latex
;(require 'ox-latex)
(when (equal system-type 'windows-nt)
  (setenv "PATH" (concat "~/Desktop/tools/texlive/bin/win32;" (getenv "PATH")))
  )

(provide 'init-org)
;;; init-org.el ends here
