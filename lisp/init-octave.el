;;; init-octave.el
(when (equal system-type 'windows-nt)
  (setq inferior-octave-program "~/Desktop/tools/octave/mingw64/bin/octave")
  )

(provide 'init-octave)
;;; init-octave.el ends here
