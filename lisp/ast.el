(setq bash-file "~/.bashrc")
(setq gitconfig-file "~/.gitconfig")
(setq gradle-file "~/.gradle/gradle.properties")

(defun switch-home()
  (interactive)
  ;; modify bashrc
  (with-temp-buffer
    (insert-file-contents bash-file)
    (set-visited-file-name bash-file)
    (while (re-search-forward "^\\(export http.+\\)" nil t)
      (replace-match "# \\1")
      )
    (save-buffer)
    )
  ;; modify gitconfig
  (with-temp-buffer
    (insert-file-contents gitconfig-file)
    (set-visited-file-name gitconfig-file)
    (while (re-search-forward "^\\(\\[url.+\\)" nil t)
      (replace-match "# \\1")
      )
    (while (re-search-forward "^\\([^#].+insteadOf.+\\)" nil t)
      (replace-match "# \\1")
      )
    (save-buffer)
    )
  ;; modify gradle.properties
  (with-temp-buffer
    (insert-file-contents gradle-file)
    (set-visited-file-name gradle-file)
    (while (re-search-forward "^\\(systemProp\\.http.+\\)" nil t)
      (replace-match "# \\1")
      )
    (save-buffer)
    )
  ;; unset proxy
  (setq url-proxy-services nil)
  )

(defun switch-onsite()
  (interactive)
  ;; modify bashrc
  (with-temp-buffer
    (insert-file-contents bash-file)
    (set-visited-file-name bash-file)
    (while (re-search-forward "^# \\(export http.+\\)" nil t)
      (replace-match "\\1")
      )
    (save-buffer)
    )
  ;; modify gitconfig
  (with-temp-buffer
    (insert-file-contents gitconfig-file)
    (set-visited-file-name gitconfig-file)
    (while (re-search-forward "^# \\(.+\\)" nil t)
      (replace-match "\\1")
      )
    (save-buffer)
    )
  ;; modify gradle.properties
  (with-temp-buffer
    (insert-file-contents gradle-file)
    (set-visited-file-name gradle-file)
    (while (re-search-forward "^# \\(systemProp\\.http.+\\)" nil t)
      (replace-match "\\1")
      )
    (save-buffer)
    )
  ;; set proxy
  (setq url-proxy-services
   '(("no_proxy" . "^\\(localhost\\|10\\..*\\|192\\.168\\..*\\)")
     ("http" . "10.214.230.152:80")
     ("https" . "10.214.230.152:80")))
  )

