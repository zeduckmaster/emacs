;;; init-elpa.el
(require 'package)

(setq package-archives
      '(("GNU ELPA"     . "https://elpa.gnu.org/packages/")
        ("MELPA Stable" . "https://stable.melpa.org/packages/")
        ("MELPA"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("MELPA Stable" . 10)
        ("GNU ELPA"     . 5)
        ("MELPA"        . 0)))

(add-to-list 'load-path (expand-file-name "elpa" user-emacs-directory))
(package-initialize)

(provide 'init-elpa)
;;; init-elpa.el ends here
