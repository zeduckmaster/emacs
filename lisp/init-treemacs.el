;;; init-treemacs.el
(require 'treemacs)

(add-hook 'treemacs-mode-hook
          (lambda ()
            (visual-line-mode -1)
            (toggle-truncate-lines 1)))

(when (display-graphic-p)
  (require 'treemacs-all-the-icons)
  (treemacs-load-theme "all-the-icons")
  )

;; projectile
(when (featurep 'projectile)
  (require 'treemacs-projectile)
  )

(treemacs-git-mode -1)

(setq treemacs-python-executable (executable-find "python3"))
(when (eq system-type 'windows-nt)
  ;;(setq treemacs-python-executable (s-replace "/" "\\" treemacs-python-executable))
  (setq treemacs-python-executable "C:\\Users\\uie62059\\AppData\\Local\\Programs\\Python\\Python39\\python.exe")
  )


(provide 'init-treemacs)
;;; init-treemacs.el ends here
