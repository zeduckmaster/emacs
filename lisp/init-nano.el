;;; init-nano.el

;; editor behavior
(setq-default
 tab-width 4
 indent-tabs-mode nil
 c-default-style "bsd"
 c-basic-offset 2
 c-indent-level 2
 mouse-wheel-scroll-amount (quote (1 ((shift) . 1) ((control))))
 )
(delete-selection-mode 1)
(ido-mode 1)
(global-set-key (kbd "C-c SPC") 'mc/edit-lines)

;; editor fonts
(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-12"))
(when (display-graphic-p)
  (set-frame-font "DejaVu Sans Mono-12" nil t)
  ;;(require 'all-the-icons)
  (setq inhibit-compacting-font-caches t)
  )

;; some cosmetics adjustments
(column-number-mode 1)
(add-hook 'prog-mode-hook
          (lambda()
            (highlight-numbers-mode 1)
            ))
(global-hl-line-mode 1)
(global-visual-line-mode 1)
(defun display-line-numbers--hook ()
  (display-line-numbers-mode 1)
  )
(add-hook 'prog-mode-hook 'display-line-numbers--hook)

;; move line up and down
(defmacro save-column (&rest body)
  `(let ((column (current-column)))
     (unwind-protect
         (progn ,@body)
       (move-to-column column))))
(put 'save-column 'lisp-indent-function 0)

(defun move-line-up ()
  (interactive)
  (save-column
    (transpose-lines 1)
    (forward-line -2)))

(defun move-line-down ()
  (interactive)
  (save-column
    (forward-line 1)
    (transpose-lines 1)
    (forward-line -1)))
(global-set-key (kbd "<M-up>") 'move-line-up)
(global-set-key (kbd "<M-down>") 'move-line-down)

;; Nano
(add-to-list 'load-path (expand-file-name "lisp/nano-emacs" user-emacs-directory))
(setq nano-font-size 12)
(setq nano-font-family-monospaced "DejaVu Sans Mono-12")
;; Theme
(require 'nano-faces)
(require 'nano-theme)
(require 'nano-theme-light)
(require 'nano-theme-dark)
(nano-theme-set-dark)
(call-interactively 'nano-refresh-theme)

;; Nano session saving (optional)
;;(require 'nano-session)

;; Nano header & mode lines (optional)
(require 'nano-modeline)

;; Nano key bindings modification (optional)
(require 'nano-bindings)

;; Compact layout (need to be loaded after nano-modeline)
;;(require 'nano-compact)
  
;; Nano counsel configuration (optional)
;; Needs "counsel" package to be installed (M-x: package-install)
;; (require 'nano-counsel)

;; Welcome message (optional)
(let ((inhibit-message t))
  (message "Welcome to GNU Emacs / N Λ N O edition")
  (message (format "Initialization time: %s" (emacs-init-time))))

;; Splash (optional)
;;(unless (member "-no-splash" command-line-args)
;;  (require 'nano-splash))

;; Help (optional)
;;(unless (member "-no-help" command-line-args)
;;  (require 'nano-help))

(provide 'init-nano)
;;; init-nano.el ends here
