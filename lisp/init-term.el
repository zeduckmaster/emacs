;;; init-term.el
(xterm-mouse-mode 1)
(setq select-enable-primary t)
(setq select-enable-clipboard t)
(setq save-interprogram-paste-before-kill t)

(provide 'init-term)
;;; init-term.el ends here
