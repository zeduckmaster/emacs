;;; init-emms.el
(require 'emms-setup)
(emms-all)

(require 'emms-player-simple)
(require 'emms-source-file)
(require 'emms-source-playlist)
(setq emms-player-list '(emms-player-mpg321
                         emms-player-ogg123
                         emms-player-mplayer))

;;(setq emms-player-list '(emms-player-vlc)
;;      emms-info-functions '(emms-info-native))

(provide 'init-emms)
;;; init-emms.el ends here
