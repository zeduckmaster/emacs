;;; init-plantuml.el
(require 'plantuml-mode)

(setq plantuml-default-exec-mode 'jar)

(add-to-list 'auto-mode-alist '("\\.uml\\'" . plantuml-mode))

(when (equal system-type 'windows-nt)
  (setq plantuml-jar-path "~/Desktop/tools/plantuml.jar")
  (setq plantuml-java-args (list "-Djava.awt.headless=true" "-jar"))
  (setq org-plantuml-jar-path (expand-file-name "~/Desktop/tools/plantuml.jar"))
  )

(provide 'init-plantuml)
;;; init-plantuml.el ends here
