;;; early-init.el --- Emacs 27+ pre-initialisation config

;;; Commentary:
;; Emacs 27+ loads this file before (normally) calling
;; `package-initialize'.  We use this file to suppress that automatic
;; behaviour so that startup is consistent across Emacs versions.
;;; Code:

(setq gc-cons-threshold 100000000)

;;(defconst *is-a-mac* (eq system-type 'darwin))
;;(defconst *is-windows* (eq system-type 'windows-nt))

(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; graphic mode customizations
(setq-default
 inhibit-splash-screen t
 inhibit-startup-screen t
 delete-by-moving-to-trash t
 )

;; y/n instead of yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;; remove temporary files next to files
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; move deleted files to trash
(setq delete-by-moving-to-trash t)

;; packages
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(setq package-archives
      '(("GNU ELPA"     . "https://elpa.gnu.org/packages/")
        ("MELPA Stable" . "https://stable.melpa.org/packages/")
        ("MELPA"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("MELPA Stable" . 10)
        ("GNU ELPA"     . 5)
        ("MELPA"        . 0)))
(add-to-list 'load-path (expand-file-name "elpa" user-emacs-directory))

;; So we can detect this having been loaded
(provide 'early-init)
;;; early-init.el ends here
