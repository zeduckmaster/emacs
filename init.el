;;; init.el --- Where all the magic begins

;; Produce backtraces when errors occur: can be helpful to diagnose startup issues
;;(setq debug-on-error t)

;; start as server
(require 'server)
(unless (server-running-p)
  (server-start))

(require 'init-locales)
;;(require 'init-nano)
(require 'init-editor)

;;(require 'init-dired)
;;(require 'init-spelling)
(require 'init-org)
(require 'init-markdown)
(require 'init-nxml)
;;(require 'init-treemacs)
;;(require 'init-clang-format)
(require 'init-octave)
;;(require 'init-elfeed)
;;(require 'init-plantuml)
;;(require 'init-latex)

;;(unless (display-graphic-p)
;;  (require 'init-term)
;;  )

;; sessions
(desktop-save-mode 0)
(require 'desktop)
;; (setq desktop-restore-forces-onscreen nil)

(if (not (daemonp))
    (desktop-save-mode 1)
  (defun restore-desktop (frame)
    "Restores desktop and cancels hook after first frame opens. 
     So the daemon can run at startup and it'll still work"
    (with-selected-frame frame
      (desktop-save-mode 1)
      (desktop-read)
      (remove-hook 'after-make-frame-functions 'restore-desktop)))
  (add-hook 'after-make-frame-functions 'restore-desktop))
;;(desktop-read)

;; ===================================================================
;; old
;;(setenv "PATH" (concat "~/Desktop/tools/mingw-w64/bin;" (getenv "PATH")))
;;(setq compile-command "mingw32-make.exe -k -j4 ")
